# README #

### General description

Linking arc-gis database to geo-dependent energy demand web-service. Allowing to extract heating energy demand for each defined rasters in arcgis.
The following script MappingDistrict.cs allows to read your arcgis database in csv format with as arguments the location of the center of your raster in CHLV02 geo referenced system.
As output the script writes back a result file with all informations required according to the information available in the WebService database.

### Set-up
* Install geo-dependent energy demand and supply WebService
* Microsoft Visual Studio
* Compile MappingDistrict.cs and all other files within Scripts folder

### Usage
The main file to be run in visual studio is MappingDistrict.cs compile in WebServiceClient project after downloading the WebServiceClient for visual studio

### More information


### Authors

Julien Marquant