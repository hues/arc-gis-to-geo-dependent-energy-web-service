﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCCER_WebServiceClient
{
    class MappingDistrict
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("{0} SCCER Web service client requires 2 arguments: username and password");
                return;
            }
            String user = args[0];
            String password = args[1];

            List<int> listMapData = new List<int>(); // data from map 
            List<string> listMapName = new List<string>(); // name of identifier data
            ReadValues(listMapData,listMapName); // get values from csv file with buildings id and location
            List<double> list = new List<double>(); // create list with results from webservice query

            for (int ii = 0; ii < (listMapData.Count)/4; ii++) // number of identifiers = 4
            {

                Console.WriteLine("Iteration:" + ii.ToString()); // write to consol information
                Console.WriteLine("count:" + listMapData.Count.ToString()); // write to consol information
                //Console.WriteLine("x:" + listMapData[(4 * ii) + 2].ToString()); // write to consol information
                //Console.Read();

                HeatDemandServiceReference.TM_WebServiceCodeSoapClient cl = new HeatDemandServiceReference.TM_WebServiceCodeSoapClient();

                //get object containing heat demand of a pixel
                // size:200 P_Xmin:540400 P_Ymin:155800


                HeatDemandServiceReference.HeatDemandPixel pDemandPixel = cl.GetHeatDemandPixel(user, password, listMapData[(4*ii)+2], listMapData[(4*ii)+3], 200);

                // Pixel information
                double P_XMin = pDemandPixel.P_XMin; // x coordinates of select pixel
                double P_YMin = pDemandPixel.P_YMin; // y coordinates of select pixel
                double P_size = pDemandPixel.pixelSize; // pixel size [m]

                // buildings characteristics
                double Bldgs_nbr = pDemandPixel.NbBuildings;  // nbr of buildings
                double Dwellings_nbr = pDemandPixel.NbAppart; // nbr of appartment
                double Dwellings_surf = pDemandPixel.TotAppSurf; // total appartment surface [m2]
                double Perc_Tot_SRE_Est_Res = pDemandPixel.Perc_Tot_SRE_Est_Res; // % of SRE used for resi. purpose
                double Perc_Tot_Ehww_EstRes = pDemandPixel.Perc_Tot_Ehww_EstRes; // % of Ehww demand for resi. purpose
                double PCover = pDemandPixel.PCover; // % of buildings with complete attributes

                string message = pDemandPixel.message; // % of buildings with complete attributes

                // heat demand aggregated for pixel
                double Tot_SRE_surf = pDemandPixel.Tot_SRE_Est; // total heated surface [m2]
                double Total_Qh_cc_Est = pDemandPixel.Tot_Qh_cc_Est; // usefull heat demand for space heating [MJ/year]
                double Total_Ehww_Est = pDemandPixel.Tot_Ehww_Est; // final energy demand for space heating and DHW production [MJ/year]

                double Total_SpecHeatDemand_Res = Total_Ehww_Est * Perc_Tot_Ehww_EstRes / Dwellings_surf; // final energy demand for space heating per square meter for residential purpose [MJ/m2.year]

                
                list.Add(P_XMin); // list output results 
                list.Add(P_YMin);
                list.Add(P_size);
                list.Add(Bldgs_nbr);
                list.Add(Dwellings_nbr);
                list.Add(Dwellings_surf);
                list.Add(Perc_Tot_SRE_Est_Res);
                list.Add(Perc_Tot_Ehww_EstRes);
                list.Add(PCover);
                list.Add(Tot_SRE_surf);
                list.Add(Total_Qh_cc_Est);
                list.Add(Total_Ehww_Est);
                list.Add(Total_SpecHeatDemand_Res); // final energy demand for space heating per square meter for residential purpose [MJ/m2.year]
                list.Add(listMapData[(4*ii)]); // ID from ArcGIS file
                
            }

            WriteValues(list);

            //Console.WriteLine("Info: Heat demand pixel:" + Total_Ehww_Est.ToString()); // write to consol information

            //string[] lines = { "x:" + P_XMin.ToString(), "y:" + P_YMin.ToString(), " pixel size:" + P_size.ToString(), "nbr of buildings:" + Bldgs_nbr.ToString(), "total heated surface [m2]" + Tot_SRE_surf.ToString() };
            //// WriteAllLines creates a file, writes a collection of strings to the file,
            //// and then closes the file.  You do NOT need to call Flush() or Close().
            //System.IO.File.WriteAllLines(@"C:\Users\maju\Desktop\WebService\WebServiceClients\Dot_Net\SCCER_WebServiceClient\output_data\Zernez_data.txt", lines);

            //Console.Read();

        }


        private static void WriteValues(List<double> args)
        {
            //Double Total_Ehww_Est = args[0];
            //Double Bldgs_nbr = args[1];

            using (var writer = new CsvFileWriter(@"C:\Users\maju\Desktop\WebService\WebServiceClients\Dot_Net\SCCER_WebServiceClient\output_data\Results_Switzerland.csv"))
            {
                // Write each row of data
                for (int row = 0; row < (args.Count)/14; row++)
                {
                    // TODO: Populate column values for this row
                    List<string> columns = new List<string>();
                    for (int ii = (14 * row); ii < (14 * row +14); ii++) // number of parameters list = 14
                    {
                        columns.Add(args[ii].ToString());
                    }

                    writer.WriteRow(columns);
                }
            }
        }

        private static void ReadValues(List<int> listMapData, List<string> listMapName)
        {
            List<string> columns = new List<string>();
            using (var reader = new CsvFileReader(@"C:\Users\maju\Desktop\WebService\WebServiceClients\Dot_Net\SCCER_WebServiceClient\output_data\RasterData_SwitzerlandAbove4.csv")) // file w/ arguments
            using (var readerName = new CsvFileReader(@"C:\Users\maju\Desktop\WebService\WebServiceClients\Dot_Net\SCCER_WebServiceClient\output_data\RoundedRasterDataLV03.csv")) // file w/ identifiers

            {
                while (reader.ReadRow(columns))
                {
                    for (int ii = 0; ii < columns.Count; ii++)
                    {
                        listMapData.Add(Convert.ToInt32(columns[ii]));
                    }
                  
                }

                while (readerName.ReadRow(columns))
                {
                    for (int ii = 0; ii < columns.Count; ii++)
                    {
                        listMapName.Add(columns[ii]);
                    }

                }
            }
        }
    }
}

